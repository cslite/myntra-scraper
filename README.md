# Myntra Scraper

- Myntra is a popular fashion e-commerce website in India.
- Data scraper written in Python with the help of BeautifulSoup.
- You can use it to scrap data like Product Image, Price, Description, Link etc. for the products available on the website in the form of tables.
