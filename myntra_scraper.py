
import os
import sys
import threading
import urllib.request, urllib.parse, urllib.error,urllib.request,urllib.error,urllib.parse,http.cookiejar
import smtplib
import ftplib
import datetime,time
import bs4
import re
import csv
import numpy
from PIL import Image
import random
from bs4 import BeautifulSoup as soup
import pandas as pd

cat_list = ['men-tshirts','men-casual-shirts','men-jackets','men-suits','men-jeans','men-casual-trousers']

for cat_name in cat_list:

  #cat_name = 'women-shrugs'
  site = "https://www.myntra.com/amp/%s?rows=500&p=1" % cat_name

  hdr = {'User-Agent': 'Mozilla/5.0 (Windows; U; MSIE 9.0; Windows NT 9.0; en-US)',
         'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
         'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
         'Accept-Encoding': 'none',
         'Accept-Language': 'en-US,en;q=0.8',
         'Connection': 'keep-alive'}
  req = urllib.request.Request(site, headers=hdr)


  cj = http.cookiejar.CookieJar()
  opener = urllib.request.build_opener(urllib.request.HTTPCookieProcessor(cj))       

  response = opener.open(req)
  content = response.read()
  response.close()


  page_soup = soup(content,"html.parser")
  xyz  = page_soup.findAll("div",{"class" : "productInfo"})
  pqr  = page_soup.findAll("div",{"class" : "product"})

  y = len(xyz)
  print(y)

  a = 2

  filename = "record_%s.csv" % cat_name
  f = open(filename,"w")
  headers = "Product_name,Brand,Current_price,Original_price\n"
  f.write(headers)

  filename2 = "file2_%s.csv" % cat_name
  opening = open(filename2,"w")
  headers1 = "Product_link,Product_img\n"
  opening.write(headers1)

  while y != 0 :
      for abc in xyz :
          product_name = abc.findAll("h4",{"class" : "name-product"})
          product_name = product_name[0].text.strip()
          product_name = str(product_name.encode('utf-8', 'replace'))
          
          brand = abc.findAll("div",{"class" : "name"})
          brand = brand[0].text
          brand = str(brand.encode('utf-8', 'replace'))

          current_price = abc.findAll("span",{"class" : "price-discounted"})
          current_price = current_price[0].text
          current_price = str(current_price.encode('utf-8', 'replace')) #solves shitty unicode problem DONOT REMOVE
          current_price = current_price[3:] #Removes first three character from converted

          original_price = str(abc.findAll("span",{"class" : "price"}))
          original_price = str([int(s) for s in original_price.split() if s.isdigit()])
          table = str.maketrans("", "", "[]")
          original_price = original_price.translate(table) #extracts brackets and places none
          #checks for string being non empty else is return if empty or secuence of white space
          if original_price and not original_price.isspace(): 
              pass
          else :
              original_price = current_price

          print(product_name)
          print(brand)
          print(current_price)
          print(original_price)
          data1 = product_name + "," + brand + "," + current_price + "," + original_price + "\n"
          f.write(data1)
      for ac in pqr:
        product_link = str(ac.a["href"])
        myntra_homepage = "https://www.myntra.com"
        product_link = myntra_homepage + product_link

        product_image = ac.findAll("amp-img")[0].get('src')       

        data3 = product_link + "," + product_image + "\n"
        opening.write(data3)

      if a == 100 :
        site = str(site[0:-2]) + str(a)
      elif a> 100:
        site = str(site[0:-3]) + str(a)
      elif a <= 10 :
        site = str(site[0:-1]) + str(a) #code will crash if the no of product listed at myntra in a specific category exceeds 500000
      elif 10 < a <=99 :
        site = str(site[0:-2]) + str(a)

      a = int(a)
      a = a+1


      req = urllib.request.Request(site, headers=hdr)
      cj = http.cookiejar.CookieJar()
      opener = urllib.request.build_opener(urllib.request.HTTPCookieProcessor(cj))
      response = opener.open(req)
      content = response.read()
      response.close()
      
      print(site)
      page_soup = soup(content,"html.parser")
      xyz  = page_soup.findAll("div",{"class" : "productInfo"})
      pqr  = page_soup.findAll("div",{"class" : "product"})
      y = len(xyz)
      print(y)


  try:
      ap = csv.reader(f, delimiter='\t')
  finally:
      f.close()                                 #just closing csv file so that it can be joined in next step

  try:
      qv = csv.reader(opening, delimiter='\t')
  finally:
      opening.close()


   